﻿using System.Text;
using System.Threading.Tasks;

namespace Slyno.Loggers
{
    /// <summary>
    /// A simple in memory logger. Call GetLog to clear the log from memory.
    /// </summary>
    public class StringBuilderLogger : Logger
    {
        private readonly StringBuilder _stringBuilder = new StringBuilder();

        public override LogType Verbosity { get; set; } = LogType.Debug;

        public override void Log(string message, LogType logType)
        {
            if ((int)logType < (int)this.Verbosity)
            {
                return;
            }

            _stringBuilder.Append(this.FormatMessage(message, logType));
        }

        public override Task LogAsync(string message, LogType logType)
        {
            this.Log(message, logType);

            return Task.FromResult<object>(null);
        }

        public string GetLog()
        {
            var log = _stringBuilder.ToString();

            _stringBuilder.Clear();

            return log;
        }
    }
}
