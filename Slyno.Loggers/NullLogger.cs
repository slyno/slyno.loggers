﻿using System.Threading.Tasks;

namespace Slyno.Loggers
{
    /// <summary>
    /// Simple noop logger.
    /// </summary>
    public sealed class NullLogger : ILogger
    {
        public LogType Verbosity { get; set; } = LogType.Debug;

        public void Log(string message, LogType logType)
        {
        }

        public Task LogAsync(string message, LogType logType)
        {
            return Task.FromResult<object>(null);
        }
    }
}
