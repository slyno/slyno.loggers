﻿namespace Slyno.Loggers
{
    public enum LogType
    {
        Debug,
        Information,
        Warning,
        Error
    }
}
