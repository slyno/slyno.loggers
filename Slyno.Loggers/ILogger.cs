﻿using System.Threading.Tasks;

namespace Slyno.Loggers
{
    public interface ILogger
    {
        LogType Verbosity { get; set; }

        void Log(string message, LogType logType);

        Task LogAsync(string message, LogType logType);
    }
}
