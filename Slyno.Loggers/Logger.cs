﻿using System;
using System.Threading.Tasks;

namespace Slyno.Loggers
{
    public abstract class Logger : ILogger
    {
        public abstract LogType Verbosity { get; set; }

        public abstract void Log(string message, LogType logType);

        public abstract Task LogAsync(string message, LogType logType);

        protected string FormatMessage(string message, LogType logType)
        {
            return $"---START---{Environment.NewLine}{logType}: {DateTime.UtcNow.ToString("u")}{Environment.NewLine}{message}{Environment.NewLine}---END---{Environment.NewLine}{Environment.NewLine}";
        }
    }
}
